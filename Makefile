compile:
	arduino-cli compile --warnings all --fqbn arduino:avr:pro

flash:
	arduino-cli upload --fqbn arduino:avr:pro --verify -v --port /dev/ttyUSB0

setup:
	wget -nc https://downloads.arduino.cc/arduino-cli/arduino-cli_latest_Linux_64bit.tar.gz
	tar xf arduino-cli_latest_Linux_64bit.tar.gz arduino-cli
	mkdir -p ~/bin
	mv arduino-cli ~/bin/
	rm arduino-cli_latest_Linux_64bit.tar.gz
	[ -e ~/.arduino15/arduino-cli.yaml ] || ~/bin/arduino-cli config init
	~/bin/arduino-cli core update-index
	~/bin/arduino-cli upgrade
	~/bin/arduino-cli core install arduino:avr
	~/bin/arduino-cli lib install 'NeoPixelBus by Makuna'
