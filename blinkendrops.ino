#include <NeoPixelBus.h>
#include <NeoPixelAnimator.h>

#define NUMBER_OF_DROPS 8
#define LEDS_PER_DROP 30

#define NEO_LEDS NUMBER_OF_DROPS * LEDS_PER_DROP
#define NEO_PIN 6

#define ANIMATIONS 2
#define RANDOM_START 20000

NeoPixelBus<NeoGrbFeature, NeoWs2812xMethod> pixels(NEO_LEDS, NEO_PIN);
NeoPixelAnimator animations(NUMBER_OF_DROPS, NEO_CENTISECONDS);

struct drop_t {
    uint8_t anim_nr;        // Which animation is running? (0 = none)
    RgbColor color1;
    RgbColor color2;
};
struct drop_t drop[NUMBER_OF_DROPS];

char buf[128];

void AnimUpdate(const AnimationParam& param)
{
    float progress = param.progress;
    uint8_t drop_nr = param.index;
    uint8_t anim_nr = drop[drop_nr].anim_nr;

    for (uint8_t i = 0; i < LEDS_PER_DROP; i++) {
        pixels.SetPixelColor(drop_nr*LEDS_PER_DROP + i, RgbColor(0,0,0));
    }
    // first animation: simple drop-down
    if (anim_nr == 0) {
        if (progress < 0.99) {
            for (uint8_t i = 0; i < (LEDS_PER_DROP / 2) * progress; i++) {
                pixels.SetPixelColor(drop_nr*LEDS_PER_DROP + i, drop[drop_nr].color1);
                pixels.SetPixelColor(drop_nr*LEDS_PER_DROP + LEDS_PER_DROP -1 - i, drop[drop_nr].color1);
            }
        } // else: clean up (i.e. keep at black)
    // second: pixel running up
    } else if (anim_nr == 1) {
        // pos goes from -1 to 15 (for 30-pixels-per-drop)
        float pos = progress * (LEDS_PER_DROP/2 + 1) - 1;
        for (uint8_t i = 0; i < LEDS_PER_DROP/2; i++) {
            // only set pixels which are near ( less than 1 away from) "pos"
            if (pos-1 < i && i < pos+1) {
                RgbColor color = RgbColor::LinearBlend(drop[drop_nr].color1, drop[drop_nr].color2, progress);
                color = color.Dim(255 * (1.0-(abs(i-pos))));
                pixels.SetPixelColor(drop_nr*LEDS_PER_DROP + LEDS_PER_DROP/2 - 1 - i, color);
                pixels.SetPixelColor(drop_nr*LEDS_PER_DROP + LEDS_PER_DROP/2 + i, color);
            }
        }
    }
}

void setup() {
    Serial.begin(115200);
    pixels.Begin();
    pixels.Show();
    randomSeed(analogRead(0));
}

void loop() {
    static uint32_t iters;
    static uint32_t start_time = millis();

    if (animations.IsAnimating()) {
        // do we maybe want to start another drop?
        for (uint8_t i = 0; i < NUMBER_OF_DROPS; i++) {
            if ( ! animations.IsAnimationActive(i) && random(RANDOM_START) == 1 ) {
                start_animation(i, random(ANIMATIONS));
            }
        }

        // the normal loop just needs these two to run the active animations
        animations.UpdateAnimations();
        pixels.Show();
    } else {
        // start a new animation NOW
        uint8_t new_drop = random(NUMBER_OF_DROPS);
        start_animation(new_drop, random(ANIMATIONS));
    }

    if (iters++ % 4000 == 100) {
        sprintf(buf, "%ld iterations in %ldms (%dfps)", iters, millis()-start_time,
                (uint16_t)(1000.0*iters/(millis()-start_time)));
        Serial.println(buf);
    }

}

void start_animation(uint8_t new_drop, uint8_t anim_nr) {
    drop[new_drop].anim_nr = anim_nr;
    RgbColor color1 = HslColor((float)random(360)/360., 0.5, 0.3);
    RgbColor color2 = HslColor((float)random(360)/360., 1.0, 0.5);
    drop[new_drop].color1 = color1;
    drop[new_drop].color2 = color2;
    animations.StartAnimation(new_drop,
            anim_nr == 0 ? random(50, 200) : random(30, 100),
            AnimUpdate);

    // debug
    sprintf(buf, "Started: drop=%d, anim=%d, color1=[%3d,%3d,%3d] color2=[%3d,%3d,%3d]",
            new_drop, anim_nr, color1.R, color1.G, color1.B, color2.R, color2.G, color2.B);
    Serial.println(buf);
}
